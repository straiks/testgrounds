import Modal from '@material-ui/core/Modal';
import { StyleRules, Theme, withStyles } from '@material-ui/core/styles';
import * as React from 'react';
import Img from '../Img';
import robo from './robo.jpg';
import './styles.module.css';

interface IErrorModalProps {
  className?: string;
  error?: string;
  open: boolean;
  classes: any;
}

function getModalStyle() {
  const top = 50;
  const left = 50;
  return {
    top: `${top}%`,
    // tslint:disable-next-line:object-literal-sort-keys
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const styles = (theme: Theme): StyleRules => ({
  paper: {
    position: 'absolute',
    width: theme.spacing.unit * 50,
    // tslint:disable-next-line:object-literal-sort-keys
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing.unit * 4,
  },
});

class ErrorModal extends React.Component<IErrorModalProps, {}> {
  public state = {
    open: this.props.open
  };

  public render() {
    const { classes } = this.props;
    return (
          <Modal
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
            open={this.state.open}
            onClose={this.handleClose}>
            <div style={getModalStyle()} className={classes.paper}>
                <div className="container">
                  <Img src={robo} className="broken" alt="Error"/>
                  {this.props.error}
                </div>
              </div>
        </Modal>
    );
  }

  public handleOpen = () => {
    this.setState({ open: true });
  };

  public handleClose = () => {
    this.setState({ open: false });
  };
}

export default withStyles(styles)(ErrorModal);
