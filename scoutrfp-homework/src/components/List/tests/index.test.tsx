import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });

import * as React from 'react';
import ListItem from '../../ListItem/index';
import List from '../index';

describe('<List />', () => {
  it('should render the component if no items are passed', () => {
    const renderedComponent = enzyme.mount(
      <List component={ListItem} />,
    );
    expect(renderedComponent.find(ListItem)).toBeTruthy();
  });

  it('should render the items', () => {
    const items = [
      'Hello',
      'World',
    ];
    const renderedComponent = enzyme.mount(
      <List items={items} component={ListItem} />,
    );
    expect(renderedComponent.containsAllMatchingElements(items.map((item) => <ListItem key={item} item={item}/>))).toBeTruthy();
  });
});
