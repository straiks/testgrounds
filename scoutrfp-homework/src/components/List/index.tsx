import * as React from 'react';

// tslint:disable-next-line:no-var-requires
const styles = require('./styles.module.css');

interface IListProps {
  component: React.ComponentClass<any> | React.StatelessComponent<any>;
  items?: any[];
}

class List extends React.Component<IListProps, {}> {
  public render() {
    const ComponentToRender = this.props.component;
    let content: JSX.Element | JSX.Element[] = (<div />);

    // If we have items, render them
    if (this.props.items && this.props.items.map) {
      content = this.props.items.map((item, index) => (
        <ComponentToRender key={`item-${index}`} item={item}/>
      ));
    } else if(this.props.items && Object.getOwnPropertyNames(this.props.items).length > 0){
      const context = this;
      content = Object.getOwnPropertyNames(this.props.items).map((item, index) => (
        <ComponentToRender key={`item-${index}`} item={{key: item, value: context.props.items != null ? context.props.items[item]: null}}/>
      ));
    }
    else {
      // Otherwise render a single component
      content = (<ComponentToRender />);
    }

    return (
      <div className={styles.listWrapper}>
        <div className={styles.list}>
          {content}
        </div>
      </div>
    );
  }
}

export default List;
