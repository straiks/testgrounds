import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as expect from 'expect';
import * as React from 'react';
import LoadingIndicator from '../index';

enzyme.configure({ adapter: new Adapter() });

describe('<LoadingIndicator />', () => {
  it('should render', () => {
    const renderedComponent = enzyme.shallow(
      <LoadingIndicator />);
    expect(renderedComponent).toBeTruthy();
  });
});
