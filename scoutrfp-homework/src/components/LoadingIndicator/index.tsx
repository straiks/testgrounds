import * as React from 'react';
import Img from '../Img';
// tslint:disable-next-line:no-var-requires
// const styles = require('./styles.module.css');
import loading from './loading.svg';
import './styles.module.css';

class LoadingIndicator extends React.Component<any, {}> {
  public render() {
    return (
      <div className="container">
          <Img src={loading} className="loading" alt="loading"/>
          Loading...
      </div>
    );
  }
}

export default LoadingIndicator;
