import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
enzyme.configure({ adapter: new Adapter() });

import * as expect from 'expect';
import * as React from 'react';
import ListItem from '../index';

describe('<ListItem />', () => {
  it('should adopt the className', () => {
    const renderedComponent = enzyme.shallow(<ListItem className="test" />);
    expect(renderedComponent.find('div').hasClass('test')).toEqual(true);
  });

  it('should render the content passed to it', () => {
    const content = 'Hello world!';
    const renderedComponent = enzyme.shallow(
      <ListItem item={content} />,
    );
    expect(renderedComponent.text()).toBe(content);
  });
});
