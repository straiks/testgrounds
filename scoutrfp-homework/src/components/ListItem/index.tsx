import * as React from 'react';

interface IListItemProps {
  className?: string;
  item?: any;
}

class ListItem extends React.Component<IListItemProps, {}> {
  public render() {
    return (
      <div className={this.props.className}>
          {this.props.item}
      </div>
    );
  }
}

export default ListItem;
