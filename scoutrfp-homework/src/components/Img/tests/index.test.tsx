import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as React from 'react';
import Img from '../index';
enzyme.configure({ adapter: new Adapter() });

const src = 'test.png';
const alt = 'test';
const renderComponent = (props = {}) => enzyme.shallow(
  <Img src={src} alt={alt} {...props} />,
);

describe('<Img />', () => {
  it('should render', () => {
    const renderedComponent = renderComponent();
    expect(renderedComponent).toBeTruthy();
  });

  it('should have an src attribute', () => {
    const renderedComponent = renderComponent().props();
    expect(renderedComponent).toHaveProperty('src');
  });

  it('should have an alt attribute', () => {
    const renderedComponent = renderComponent().props();
    expect(renderedComponent).toHaveProperty('alt', alt);
  });

  it('should adopt a className attribute', () => {
    const className = 'test';
    const renderedComponent = renderComponent({ className });
    expect(renderedComponent.hasClass(className)).toEqual(true);
  });
});
