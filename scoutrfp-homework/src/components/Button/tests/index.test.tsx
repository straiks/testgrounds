import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as expect from 'expect';
import * as React from 'react';

import Button from '../index';
enzyme.configure({ adapter: new Adapter() });

describe('<Button />', () => {
  it('should render its children', () => {
    const children = (<h1>Test</h1>);
    const renderedComponent = enzyme.shallow(
      <Button href="http://scoutrfp.com">
        {children}
      </Button>,
    );
    expect(renderedComponent.contains(children)).toEqual(true);
  });

  it('should adopt the className', () => {
    const renderedComponent = enzyme.shallow(<Button className="test" />);
    expect(renderedComponent.find('a').hasClass('test')).toEqual(true);
  });

  it('should render an <a> tag if no route is specified', () => {
    const renderedComponent = enzyme.shallow(<Button href="http:/scoutrfp.com" />);
    expect(renderedComponent.find('a').length).toEqual(1);
  });

  it('should render a button to change route if the handleRoute prop is specified', () => {
    function handler() { /* empty */ }
    const renderedComponent = enzyme.shallow(<Button handleRoute={handler} />);

    expect(renderedComponent.find('button').length).toEqual(1);
  });
});
