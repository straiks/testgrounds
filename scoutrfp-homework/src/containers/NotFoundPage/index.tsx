/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 */

import * as React from 'react';
import { connect } from 'react-redux';


interface INotFoundProps {
  dispatch?: (action: any) => void;
}

export class NotFound extends React.Component<INotFoundProps, {}> {

  constructor(props: any) {
    super(props);
  }

  public render() {
    return (
      <article>
        PAGE NOT FOUND
      </article>
    );
  }
}

// Wrap the component to inject dispatch and state into it
export default connect()(NotFound);
