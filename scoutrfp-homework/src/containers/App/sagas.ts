/**
 * Gets the rates
 */

// tslint:disable-next-line:ordered-imports
import { LOCATION_CHANGE } from 'react-router-redux';
import { delay } from 'redux-saga';
import { call, cancel, fork, put, select, take } from 'redux-saga/effects';
import { ratesLoaded, ratesLoadingError } from './actions';
import { LOAD_RATES } from './constants';

import request from '../../utils/request';
import { selectFakeEndpointEnabled, selectTimeoutEnabled } from './selectors';

/**
 * Rates request/response handler
 */
export function* getRates(): IterableIterator<any> {
    const timeoutEnabled = yield select(selectTimeoutEnabled());
    const endpointDisabled = yield select(selectFakeEndpointEnabled());
    if(timeoutEnabled) {
      yield call(delay, 2000);
    }
    const requestURL = endpointDisabled ? `https://api.fakeendpoint.io` : `https://api.exchangeratesapi.io/latest`;

    // Call request helper (see 'app/utils/request')
    const rates = yield call(request, requestURL);
    if (!rates.err) {
      yield put(ratesLoaded(rates.data));
    } else {
      yield put(ratesLoadingError(rates.err));
    }
}

/**
 * Watches for LOAD_RATES action and calls handler
 */
export function* getRatesWatcher(): IterableIterator<any> {
  while (yield take(LOAD_RATES)) {
    yield call(getRates);
  }
}

// /**
//  * Root saga manages watcher lifecycle
//  */
export function* ratesData(): IterableIterator<any> { 
  // Fork watcher so we can continue execution
  const watcher = yield fork(getRatesWatcher);

  // Suspend execution until location changes
  yield take(LOCATION_CHANGE);
  yield cancel(watcher);
}

// Bootstrap sagas
export default [
  ratesData,
];
