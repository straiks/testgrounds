/**
 * Homepage selectors
 */

import { createSelector } from 'reselect';

const selectGlobal = () => (state: any) => state.get('home');

const selectLoading = () => createSelector(
  selectGlobal(),
  (globalState: any) => globalState.get('loading'),
);
const selectTimeoutEnabled = () => createSelector(
  selectGlobal(),
  (globalState: any) => globalState.get('timeoutEnabled'),
);

const selectFakeEndpointEnabled = () => createSelector(
  selectGlobal(),
  (globalState: any) => globalState.get('fakeEndpointEnabled'),
);

const selectError = () => createSelector(
  selectGlobal(),
  (globalState: any) => globalState.get('error'),
);

const selectRates = () => createSelector(
  selectGlobal(),
  (globalState: any) => globalState.getIn(['ratesData', 'rates']),
);

export {
  selectGlobal,
  selectLoading,
  selectError,
  selectRates,
  selectTimeoutEnabled,
  selectFakeEndpointEnabled
};
