/*
 * AppReducer
 *
 * The reducer takes care of our data. Using actions, we can change our
 * application state.
 * To add a new action, add it to the switch statement in the reducer function
 *
 * Example:
 * case YOUR_ACTION_CONSTANT:
 *   return state.set('yourStateVariable', true);
 */
import {  
  ENABLE_DISABLE_FAKE_ENDPOINT,
  ENABLE_DISABLE_TIMEOUT,
  LOAD_RATES,
  LOAD_RATES_FAILURE,
  LOAD_RATES_SUCCESS
} from './constants';

import { fromJS } from 'immutable';

// The initial state of the App
const initialState = fromJS({
  loading: false,
  // tslint:disable-next-line:object-literal-sort-keys
  error: false,
  ratesData: fromJS({
    rates: false,
    // tslint:disable-next-line:object-literal-sort-keys
    date: '',
    base:''
  }),
  endpointDisabled: false,
  timeoutEnabled: false
});

function homeReducer(state = initialState, action: any) {
  switch (action.type) {
    case LOAD_RATES:
      return state
        .set('loading', true)
        .set('error', false)
        .setIn(['ratesData', 'rates'], false);
    case LOAD_RATES_SUCCESS:
      return state
        .setIn(['ratesData', 'rates'], action.rates.rates)
        .setIn(['ratesData', 'date'], action.rates.date)
        .setIn(['ratesData', 'base'], action.rates.base)
        .set('loading', false)
    case LOAD_RATES_FAILURE:
      return state
        .set('error', action.error)
        .set('loading', false);
    case ENABLE_DISABLE_FAKE_ENDPOINT:
      return state
        .set('fakeEndpointEnabled', action.enabled);
    case ENABLE_DISABLE_TIMEOUT:
        return state
          .set('timeoutEnabled', action.enabled);
    default:
      return state;
  }
}

export default homeReducer;
