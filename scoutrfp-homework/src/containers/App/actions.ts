import {
  ENABLE_DISABLE_FAKE_ENDPOINT,
  ENABLE_DISABLE_TIMEOUT,
  LOAD_RATES,
  LOAD_RATES_FAILURE,
  LOAD_RATES_SUCCESS
} from './constants';


/**
 * Load the rates, this action starts the request saga
 *
 * @return {object} An action object with a type of LOAD_RATES
 */
export const loadRates = (payload:any) => {
  return {
    type: LOAD_RATES,
    ...payload
  };
};
/**
 * Dispatched when the rates are loaded by the request saga
 *
 * @param  {array} rates The rates data
 * @param  {string} username The current username
 *
 * @return {object} An action object with a type of LOAD_RATES_SUCCESS passing the rates
 */
export function ratesLoaded(rates: any) {
  return {
    type: LOAD_RATES_SUCCESS,
    // tslint:disable-next-line:object-literal-sort-keys
    rates
  };
}

/**
 * Dispatched when loading the repositories fails
 *
 * @param  {object} error The error
 *
 * @return {object}       An action object with a type of LOAD_REPOS_ERROR passing the error
 */
export function ratesLoadingError(error: any) {
  return {
    type: LOAD_RATES_FAILURE,
    // tslint:disable-next-line:object-literal-sort-keys
    error,
  };
}

export const enableDisableFakeEndpoint = (enabled: boolean) => {
  return {
    type: ENABLE_DISABLE_FAKE_ENDPOINT,
    // tslint:disable-next-line:object-literal-sort-keys
    enabled
  };
};

export const enableDisableTimeout = (enabled: boolean) => {
  return {
    type: ENABLE_DISABLE_TIMEOUT,
    // tslint:disable-next-line:object-literal-sort-keys
    enabled
  };
};