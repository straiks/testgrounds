import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormGroup from '@material-ui/core/FormGroup';
import Switch from '@material-ui/core/Switch';
import * as React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import ErrorModal from '../../components/ErrorModal';
import Img from '../../components/Img';
import './App.css';
import logo from './scoutrfp.png';

import {
  selectError,
  selectFakeEndpointEnabled,
  selectLoading,
  selectRates,
  selectTimeoutEnabled,
} from './selectors';

import { 
  enableDisableFakeEndpoint,
  enableDisableTimeout, 
  loadRates } from './actions';

import Button from '../../components/Button';
import LoadingIndicator from '../../components/LoadingIndicator';
import RatesListItem from '../../containers/RatesListItem';


import List from '../../components/List';

// tslint:disable-next-line:no-var-requires
import './styles.module.css';

interface IHomePageProps {
  timeoutEnabled?: boolean;
  fakeEndpointEnabled?:boolean;
  loading?: boolean;
  error?: Error | false;
  rates?: any[];
  onLoadRates?: () => React.EventHandler<any>;
  onTimeoutEnabled?: (enabled: boolean) => React.EventHandler<any>;
  onFakeEndpointEnabled?: (enabled: boolean) => React.EventHandler<any>;
}

export class HomePage extends React.Component<IHomePageProps, {}> {
  /**
   * when initial state username is not null, submit the form to load repos
   */
   public componentDidMount() {
    // if (this.props.username && this.props.username.trim().length > 0) {
    //   this.props.onSubmitForm();
    // }
  }

   public render() {
    let mainContent = null;

    // Show a loading indicator when we're loading
    if (this.props.loading) {
      mainContent = (<List component={LoadingIndicator} />);

      // Show an error if there is one
    } else if (this.props.error !== false) {
      // const ErrorComponent = () => (
      //   <ListItem item={'Something went wrong, please try again!'} />
      // );
      mainContent = (<ErrorModal error='Something went wrong, please try again!' open={true} />);

      // If we're not loading, don't have an error and there are repos, show the repos
    } else if (this.props.rates) {
      mainContent = (<List items={this.props.rates} component={RatesListItem} />);
    }

    return (
      <div className="App">
        <header className="App-header">
          <Img src={logo} className="App-logo" alt="logo"/>
        </header>
         <Button onClick={this.getRates}>
            Load rates
          </Button>
          <div className="table-wrapper">
            <div className="left"/>
              <div className="content">
                {mainContent}
              </div>
                <div className="right">
                <FormGroup row={true} className="pull-right">
                  <FormControlLabel
                    control={
                      <Switch value={this.props.timeoutEnabled}
                        checked={this.props.timeoutEnabled}
                        onChange={this.setTimeoutEnabled()}
                        color="primary"
                        />
                    }
                    label="add timeout"
                  />
                  <FormControlLabel
                    control={
                      <Switch value={this.props.fakeEndpointEnabled}
                        checked={this.props.fakeEndpointEnabled}
                        onChange={this.setFakeEndpointEnabled()}
                        color="secondary"
                        />
                    }
                    label="disable endpoint"
                  />
              </FormGroup>
            </div>
          </div>
      </div>
    );
  }

  public setFakeEndpointEnabled = () => (event:any) => {
    if (this.props.onFakeEndpointEnabled) {
      this.props.onFakeEndpointEnabled(event.target.checked);
    }
  };

  public setTimeoutEnabled = () => (event:any) => {
    if (this.props.onTimeoutEnabled) {
      this.props.onTimeoutEnabled(event.target.checked);
    }
  };

  private getRates = () => {
    if(this.props.onLoadRates)
    {
      this.props.onLoadRates();
    }
  }
}

export function mapDispatchToProps(dispatch: any) {
  return {
    onLoadRates: (evt: any) => {
      if (evt !== undefined && evt.preventDefault) { evt.preventDefault(); }
      dispatch(loadRates(null));
    },
    // tslint:disable-next-line:object-literal-sort-keys
    onFakeEndpointEnabled: (enabled: boolean) => {
      dispatch(enableDisableFakeEndpoint(enabled));
    },
    onTimeoutEnabled: (enabled: boolean) => {
      dispatch(enableDisableTimeout(enabled));
    },
    dispatch,
  };
}

const mapStateToProps = createStructuredSelector({
  rates: selectRates(),
  // tslint:disable-next-line:object-literal-sort-keys
  loading: selectLoading(),
  error: selectError(),
  timeoutEnabled: selectTimeoutEnabled(),
  fakeEndpointEnabled: selectFakeEndpointEnabled()
});

// Wrap the component to inject dispatch and state into it
export default connect<{}, {}, IHomePageProps>(mapStateToProps, mapDispatchToProps)(HomePage);
