const prefix = 'APP/HOME/';
export const LOAD_RATES = `${prefix}LOAD_RATES`;
export const LOAD_RATES_SUCCESS = `${prefix}LOAD_RATES_SUCCESS`;
export const LOAD_RATES_FAILURE = `${prefix}LOAD_RATES_FAILURE`;

export const ENABLE_DISABLE_TIMEOUT = `${prefix}ENABLE_DISABLE_TIMEOUT`;
export const ENABLE_DISABLE_FAKE_ENDPOINT = `${prefix}ENABLE_DISABLE_FAKE_ENDPOINT`;

