import * as expect from 'expect';
import { fromJS } from 'immutable';
import homeReducer from '../reducer';

describe('homeReducer', () => {
  let state: any;
  beforeEach(() => {
    state = fromJS({
      loading: false,
      // tslint:disable-next-line:object-literal-sort-keys
      error: false,
      ratesData: fromJS({
        rates: false,
        // tslint:disable-next-line:object-literal-sort-keys
        date: '',
        base:''
      }),
      endpointDisabled: false,
      timeoutEnabled: false
    });
  });

  it('should return the initial state', () => {
    const expectedResult = state;
    expect(homeReducer(undefined, {})).toEqual(expectedResult);
  });
});
