import * as expect from 'expect';
import { LOCATION_CHANGE } from 'react-router-redux';
import { Action } from 'redux';
import { call, fork, put, take } from 'redux-saga/effects';
import request from '../../../utils/request';
import { LOAD_RATES } from '../constants';
import { getRates, getRatesWatcher, ratesData } from '../sagas';

describe('getRates Saga', () => {
  let getRatesGenerator: any;

  // We have to test twice, once for a successful load and once for an unsuccessful one
  // so we do all the stuff that happens beforehand automatically in the beforeEach
  beforeEach(() => {
    getRatesGenerator = getRates();
    const requestURL = `https://api.exchangeratesapi.io/latest`;
     const callDescriptor = getRatesGenerator.next().value;
    expect(callDescriptor).toEqual(call(request, requestURL));
  });
});

describe('getRatesWatcher Saga', () => {
  const getRatesWatcherGenerator = getRatesWatcher();

  it('should watch for LOAD_RATES action', () => {
    const takeDescriptor = getRatesWatcherGenerator.next().value;
    expect(takeDescriptor).toEqual(take(LOAD_RATES));
  });

  it('should invoke getRates saga on actions', () => {
    const callDescriptor = getRatesWatcherGenerator.next(put(LOAD_RATES as any as Action)).value; 
    expect(callDescriptor).toEqual(call(getRates));
  });
});

describe('ratesDataSaga Saga', () => {
  const ratesDataSaga = ratesData();
  let forkDescriptor;

  it('should asyncronously fork getRatesWatcher saga', () => {
    forkDescriptor = ratesDataSaga.next();
    expect(forkDescriptor.value).toEqual(fork(getRatesWatcher));
  });
  
  it('should yield until LOCATION_CHANGE action', () => {
    const takeDescriptor = ratesDataSaga.next();
    expect(takeDescriptor.value).toEqual(take(LOCATION_CHANGE));
  });
});
