import * as expect from 'expect';
import { fromJS } from 'immutable';

import {
  selectError,
  selectGlobal,
  selectLoading,
  selectRates
} from '../selectors';

describe('selectHome', () => {
  const globalSelector = selectGlobal();
  it('should select the home state', () => {
    const globalState = fromJS({});
    const mockedState = fromJS({
      home: globalState,
    });
    expect(globalSelector(mockedState)).toEqual(globalState);
  });
});

describe('selectLoading', () => {
  const loadingSelector = selectLoading();
  it('should select the loading', () => {
    const loading = true;
    const mockedState = fromJS({
      home: {
        loading,
      },
    });
    expect(loadingSelector(mockedState)).toEqual(loading);
  });
});

describe('selectRates', () => {
  const ratesSelector = selectRates();
  it('should select rates', () => {
    const ratesFixture = {USD:1};
    const ratesData = fromJS({
      rates: ratesFixture,
    });
    const mockedState = fromJS({
      home: {
        ratesData,
      },
    });
    expect(ratesSelector(mockedState)).toEqual(fromJS(ratesFixture));
  });
});

describe('selectError', () => {
  const errorSelector = selectError();
  it('should select the error', () => {
    const error = 'error';
    const mockedState = fromJS({
      home: {
        error,
      },
    });
    expect(errorSelector(mockedState)).toEqual(error);
  });
});
