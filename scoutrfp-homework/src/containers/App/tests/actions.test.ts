import * as expect from 'expect';

import {
  LOAD_RATES,
} from '../constants';

import {
  loadRates,
} from '../actions';

describe('App Actions', () => {
  describe('loadRates', () => {
    it('should return the correct type', () => {
      const expectedResult = {
        type: LOAD_RATES,
      };
      expect(loadRates(null)).toEqual(expectedResult);
    });
  });
});
