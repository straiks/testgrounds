/**
 * Test the HomePage
 */
import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as expect from 'expect';

import * as React from 'react';
import List from '../../../components/List';
import LoadingIndicator from '../../../components/LoadingIndicator';
import RatesListItem from '../../RatesListItem';
import { HomePage } from '../index';

enzyme.configure({ adapter: new Adapter() });

describe('<HomePage />', () => {
  it('should render the loading indicator when its loading', () => {
    const renderedComponent = enzyme.shallow(
      <HomePage loading={true} />,
    );
    expect(renderedComponent.contains(<List component={LoadingIndicator} />)).toEqual(true);
  });
  it('should render the rates if loading was successful', () => {
    const ratesFixture = [{
     date: new Date(),
     // tslint:disable-next-line:object-literal-sort-keys
     base: 'EUR',
     rates:{
       USD:1,
       // tslint:disable-next-line:object-literal-sort-keys
       EUR:2
     }
    }];
    const renderedComponent = enzyme.shallow(
      <HomePage
        rates={ratesFixture}
        error={false}
      />,
    );

    expect(renderedComponent.contains(<List items={ratesFixture} component={RatesListItem} />)).toEqual(true);
  });

});
