
import * as React from 'react';
import ListItem from '../../components/ListItem';

// tslint:disable-next-line:no-var-requires
import './styles.module.css';

interface IRatesListItemProps {
  item: any;
}

export class RatesListItem extends React.Component<IRatesListItemProps, {}> { // eslint-disable-line react/prefer-stateless-function
  public render() {
    const item = this.props.item;
    // Put together the content of the rates
    const content = (
      <div className="wrapper">
          <div className="key">{item.key}</div>
          <div className="value">{item.value}</div>
      </div>
    );
    // Render the content into a list item
    return (
      <ListItem key={`rates-list-item-${item.full_name}`} item={content} />
    );
  }
}

// Wrap the component to inject dispatch and state into it
export default RatesListItem;