import * as enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import * as expect from 'expect';
import * as React from 'react';

import ListItem from '../../../components/ListItem';
import RatesListItem from '../index';

enzyme.configure({ adapter: new Adapter() });

describe('<RatesListItem />', () => {
  let item: any;

  // Before each test reset the item data for safety
  beforeEach(() => {
    item = {
      key: 'EUR',
      value: 1
    };
  });

  it('should render a ListItem', () => {
    const renderedComponent = enzyme.shallow(
      <RatesListItem item={item} />,
    );
    expect(renderedComponent.find(ListItem).length).toEqual(1);
  });


  it('should render the rate key', () => {
    const renderedComponent = enzyme.mount(
        <RatesListItem item={item} />
    );
    expect(renderedComponent.text().indexOf(item.key)).toBeGreaterThan(-1);
  });

  it('should render the rate value', () => {
    const renderedComponent = enzyme.mount(
        <RatesListItem item={item} />
    );
    expect(renderedComponent.text().indexOf(item.value)).toBeGreaterThan(-1);
  });
});
