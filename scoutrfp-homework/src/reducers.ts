
import { fromJS } from 'immutable';
import { LOCATION_CHANGE } from 'react-router-redux';
import { Reducer, ReducersMapObject } from 'redux';
import { combineReducers } from 'redux-immutable';
import homeReducer from './containers/App/reducer';

/*
 * routeReducer
 *
 * The reducer merges route location changes into our immutable state.
 * The change is necessitated by moving to react-router-redux@4
 *
 */

// Initial routing state
const routeInitialState = fromJS({
  locationBeforeTransitions: null,
});

/**
 * Merge route into the global application state
 */
function routeReducer(state = routeInitialState, action: any) {
  switch (action.type) {
    /* istanbul ignore next */
    case LOCATION_CHANGE:
      const mergeState = state.merge({
        locationBeforeTransitions: action.payload,
      });
      // if (window.swUpdate) {
      //   window.location.reload();
      // }
      return mergeState;
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default function createReducer(asyncReducers: ReducersMapObject = {}): Reducer<any> {

  const reducers = {
    route: routeReducer,
    // tslint:disable-next-line:object-literal-sort-keys
    home: homeReducer,
    ...asyncReducers,
  };

  return combineReducers(reducers);
}
