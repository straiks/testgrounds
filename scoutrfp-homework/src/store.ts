/**
 * Create the store with asynchronously loaded reducers
 */

import { fromJS } from 'immutable';
// import { routerMiddleware } from 'react-router-redux';
import { Action, applyMiddleware, compose, createStore, Middleware, ReducersMapObject, Store } from 'redux';
import createSagaMiddleware from 'redux-saga';
import createReducer from './reducers';
const sagaMiddleware = createSagaMiddleware();
import {SagaIterator, Task} from 'redux-saga';

export interface IStore<T> extends Store<T> {
  runSaga?: (saga: (...args: any[]) => SagaIterator, ...args: any[]) => Task;
  asyncReducers?: ReducersMapObject;
}
declare interface IWindow extends Window {
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: typeof compose;
}
declare const window: IWindow;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore<T>(initialState: object = {}, history: any): IStore<T> {
  // Create the store with middlewares
  // 1. sagaMiddleware: Makes redux-sagas work
  // 2. routerMiddleware: Syncs the location/URL path to the state
  const middlewares: Middleware[] = [
    sagaMiddleware,
    // routerMiddleware(history),
  ];

  const store: IStore<T> = createStore<T, Action<any>, {}, {}>(
    createReducer(),
    fromJS(initialState),
    composeEnhancers(
      applyMiddleware(...middlewares),
    ),
  );

  // Extensions
  store.runSaga = sagaMiddleware.run;
  store.asyncReducers = {}; // Async reducer registry

  return store;
}
