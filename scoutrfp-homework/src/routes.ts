// These are the pages you can go to.
// They are all wrapped in the App component, which should contain the navbar etc
// See http://blog.mxstbr.com/2016/01/react-apps-with-pages for more information
// about the code splitting business

import { RouteProps } from 'react-router';
import { getAsyncInjectors } from './utils/asyncInjectors';

const errorLoading = (err: any) => {
  // tslint:disable-next-line:no-console
  console.error('Dynamic page loading failed', err); // eslint-disable-line no-console
};

const loadModule = (cb: any) => (componentModule: any) => {
  cb(null, componentModule.default);
};

export interface IExtendedRouteProps extends RouteProps {
  name?: string;
  path: string;
  getComponent:any;
}

export default function createRoutes(store: any): IExtendedRouteProps[] {
  // create reusable async injectors using getAsyncInjectors factory
  const { injectReducer, injectSagas } = getAsyncInjectors(store);

  return [
    {
      path: '/',
      // tslint:disable-next-line:object-literal-sort-keys
      name: 'home',
      getComponent(nextState: any, cb: any) {
        const importModules = Promise.all([
          System.import('./containers/App/reducer'),
          System.import('./containers/App/sagas'),
          System.import('./containers/App/index'),
        ]);
        const renderRoute = loadModule(cb);
        importModules.then(([reducer, sagas, component]) => {
          // tslint:disable-next-line:no-debugger
          injectReducer('home', reducer.default);
          injectSagas(sagas.default);
          renderRoute(component);
        });

        importModules.catch(errorLoading);
      },
    }, {
      path: '*',
      // tslint:disable-next-line:object-literal-sort-keys
      name: 'notfound',
      getComponent(nextState: any, cb: any) {
        System.import('./containers/NotFoundPage/index')
          .then(loadModule(cb))
          .catch(errorLoading);
      },
    },
  ];
}
