import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import { browserHistory, Router } from 'react-router';

import App from './containers/App';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import createRoutes from './routes';
import configureStore from './store';

// Create redux store with history
// this uses the singleton browserHistory provided by react-router
// Optionally, this could be changed to leverage a created history
// e.g. `const browserHistory = useRouterHistory(createBrowserHistory)();`
const initialState = {};
const store = configureStore(initialState, browserHistory);


// Set up the router, wrapping all Routes in the App component
const rootRoute = {
  component: App,
  // tslint:disable-next-line:object-literal-sort-keys
  childRoutes: createRoutes(store),
};

ReactDOM.render(
  <Provider store={store}>
    <Router history={browserHistory} routes={rootRoute}/>
  </Provider>,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
